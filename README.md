# SWA - Semestral work - BorrowReturnService

Author: Oleg Baryshnikov


## Description
This application works as one of microservices from the root project - https://gitlab.fel.cvut.cz/barysole/swa-root .
Current borrow/return service provides functions to borrow, return books, reserve and remove books reservation and store all operations in the database. Microservice works together with other services by using the "openfeign" library and "eurika" service register. It uses a book catalog service (https://gitlab.fel.cvut.cz/barysole/swa-catalog-service) to get and edit books data. Also the application uses user service (https://gitlab.fel.cvut.cz/barysole/swa-user-service) to retrieve user information. After application start, the service tries to register themself to the eureka server which has port "8236". After that microservice listens to requests at endpoints described at (where?). Also the app connected with their own postgreSQL database (described more precisely in the root project).
## Documentation
Api documentation will be available at http://localhost:8234/swagger-ui/index.html after the app starts.
## GitLab pipeline
Project contains their own GitLab pipeline described in .gitlab-ci.yml. Gitlab pipeline contains three stages: build, test and building image.

Stage description:
- On a build stage the runner builds jar artefacts and stores them for further uses.
- On a test stage the runner runs an application unit-test.
- On a building stage runner build docker-image and push it to following private repository: https://hub.docker.com/repository/docker/verminaardo/swa-baryshnikov-borrow-return-service
## Most important app properties
```
spring.application.name=SwaBorrowReturnService
server.port=8234
```