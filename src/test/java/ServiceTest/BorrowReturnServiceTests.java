package ServiceTest;

import cz.cvut.fel.barysole.SwaBorrowReturnServiceApplication;
import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.service.BookManipulationService;
import feign.FeignException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SwaBorrowReturnServiceApplication.class)
public class BorrowReturnServiceTests {

    @Autowired
    private BookManipulationService bookManipulationService;

    @Test
    public void request_with_disabled_microservices_must_throw_service_unavailable_exception() {
        Assertions.assertThrows(FeignException.FeignClientException.ServiceUnavailable.class, () -> bookManipulationService.reserveBook(new BookAndUserPair()));
    }
}
