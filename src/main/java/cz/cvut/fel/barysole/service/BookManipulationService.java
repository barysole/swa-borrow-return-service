package cz.cvut.fel.barysole.service;

import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.pojo.dto.BookDTO;

public interface BookManipulationService {

    BookDTO borrowBook(BookAndUserPair bookAndUserPair);

    BookDTO returnBook(BookAndUserPair bookAndUserPair);

    BookDTO reserveBook(BookAndUserPair bookAndUserPair);

    BookDTO removeReservation(BookAndUserPair bookAndUserPair);

}
