package cz.cvut.fel.barysole.service;

import cz.cvut.fel.barysole.client.BookCatalogServiceClient;
import cz.cvut.fel.barysole.client.UserServiceClient;
import cz.cvut.fel.barysole.enums.BookManipulationOperation;
import cz.cvut.fel.barysole.enums.BookStatus;
import cz.cvut.fel.barysole.exceptions.BadRequest;
import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import cz.cvut.fel.barysole.pojo.dto.UserDTO;
import cz.cvut.fel.barysole.pojo.entity.BookManipulationInformation;
import cz.cvut.fel.barysole.repository.BookManipulationRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BookManipulationServiceImpl implements BookManipulationService {

    private final BookCatalogServiceClient bookCatalogServiceClient;
    private final BookManipulationRepository bookManipulationRepository;
    private final UserServiceClient userServiceClient;

    public BookManipulationServiceImpl(UserServiceClient userServiceClient, BookManipulationRepository bookManipulationRepository, BookCatalogServiceClient bookCatalogServiceClient) {
        this.userServiceClient = userServiceClient;
        this.bookManipulationRepository = bookManipulationRepository;
        this.bookCatalogServiceClient = bookCatalogServiceClient;
    }

    private String createFioAndLoginString(UserDTO userDTO) {
        return userDTO.getFio() + " (" + userDTO.getLogin() + ")";
    }

    @Override
    @Transactional
    public BookDTO borrowBook(BookAndUserPair bookAndUserPair) {
        UserDTO userDTO = userServiceClient.getUser(bookAndUserPair.getUserId());
        BookDTO bookDTO = bookCatalogServiceClient.getBook(bookAndUserPair.getBookId());
        if ((bookDTO.getBookStatus().equals(BookStatus.RESERVED) || bookDTO.getBookStatus().equals(BookStatus.AVAILABLE)) &&
                (
                        bookDTO.getReservedBy() == null ||
                                bookDTO.getReservedBy().isEmpty() ||
                                createFioAndLoginString(userDTO).equals(bookDTO.getReservedBy())
                )
        ) {
            BookManipulationInformation bookManipulationInformation = new BookManipulationInformation();
            bookManipulationInformation.setBookId(bookDTO.getId());
            bookManipulationInformation.setUserId(userDTO.getId());
            bookManipulationInformation.setOperation(BookManipulationOperation.BORROWED_BY_USER);
            bookManipulationRepository.save(bookManipulationInformation);
            bookDTO.setBookStatus(BookStatus.BORROWED);
            bookDTO.setBorrowedBy(createFioAndLoginString(userDTO));
            bookDTO.setReservedBy(null);
            return bookCatalogServiceClient.saveBook(bookDTO);
        } else {
            throw new BadRequest("Book cant be borrowed by this user.");
        }

    }

    @Override
    public BookDTO returnBook(BookAndUserPair bookAndUserPair) {
        BookDTO bookDTO = bookCatalogServiceClient.getBook(bookAndUserPair.getBookId());
        if (bookDTO.getBookStatus().equals(BookStatus.BORROWED)) {
            BookManipulationInformation bookManipulationInformation = new BookManipulationInformation();
            bookManipulationInformation.setBookId(bookDTO.getId());
            bookManipulationInformation.setUserId(bookAndUserPair.getUserId());
            bookManipulationInformation.setOperation(BookManipulationOperation.RETURNED_BY_USER);
            bookManipulationRepository.save(bookManipulationInformation);
            bookDTO.setBookStatus(BookStatus.AVAILABLE);
            bookDTO.setBorrowedBy("");
            return bookCatalogServiceClient.saveBook(bookDTO);
        } else {
            throw new BadRequest("Book cant be returned.");
        }
    }

    @Override
    public BookDTO reserveBook(BookAndUserPair bookAndUserPair) {
        UserDTO userDTO = userServiceClient.getUser(bookAndUserPair.getUserId());
        BookDTO bookDTO = bookCatalogServiceClient.getBook(bookAndUserPair.getBookId());
        if (bookDTO.getBookStatus().equals(BookStatus.AVAILABLE)) {
            BookManipulationInformation bookManipulationInformation = new BookManipulationInformation();
            bookManipulationInformation.setBookId(bookDTO.getId());
            bookManipulationInformation.setUserId(userDTO.getId());
            bookManipulationInformation.setOperation(BookManipulationOperation.RESERVED_BY_USER);
            bookManipulationRepository.save(bookManipulationInformation);
            bookDTO.setBookStatus(BookStatus.RESERVED);
            bookDTO.setReservedBy(userDTO.getFio() + " (" + userDTO.getLogin() + ")");
            return bookCatalogServiceClient.saveBook(bookDTO);
        } else {
            throw new BadRequest("Book cant be reserved.");
        }
    }

    @Override
    public BookDTO removeReservation(BookAndUserPair bookAndUserPair) {
        UserDTO userDTO = userServiceClient.getUser(bookAndUserPair.getUserId());
        BookDTO bookDTO = bookCatalogServiceClient.getBook(bookAndUserPair.getBookId());
        if (bookDTO.getBookStatus().equals(BookStatus.RESERVED) &&
                (
                        bookDTO.getReservedBy() == null ||
                                bookDTO.getReservedBy().isEmpty() ||
                                createFioAndLoginString(userDTO).equals(bookDTO.getReservedBy())
                )
        ) {
            BookManipulationInformation bookManipulationInformation = new BookManipulationInformation();
            bookManipulationInformation.setBookId(bookDTO.getId());
            bookManipulationInformation.setUserId(bookAndUserPair.getUserId());
            bookManipulationInformation.setOperation(BookManipulationOperation.RESERVATION_REMOVED_BY_USER);
            bookManipulationRepository.save(bookManipulationInformation);
            bookDTO.setBookStatus(BookStatus.AVAILABLE);
            bookDTO.setReservedBy(null);
            return bookCatalogServiceClient.saveBook(bookDTO);
        } else {
            throw new BadRequest("Reservation can not be removed by this user.");
        }
    }
}
