package cz.cvut.fel.barysole.repository;

import cz.cvut.fel.barysole.pojo.entity.BookManipulationInformation;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookManipulationRepository extends PagingAndSortingRepository<BookManipulationInformation, String> {
}

