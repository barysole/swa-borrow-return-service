package cz.cvut.fel.barysole.pojo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private String id;

    private String login;

    private String fio;

    private String firstName;

    private String lastName;

    private String middleName;

    private String email;

    private String telephone;

    private String password;

}
