package cz.cvut.fel.barysole.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BookAndUserPair {

    @Schema(description = "Existing user id", example = "0e9054f7-c0be-4e2e-b050-a625874818f7", required = true)
    private String userId;

    @Schema(description = "Existing book id", example = "7c0b82de-220d-4ce3-807a-99f91e223143", required = true)
    private String bookId;

}
