package cz.cvut.fel.barysole.pojo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.barysole.enums.BookStatus;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDTO {

    private String id;

    private String title;

    private String author;

    private BookStatus bookStatus;

    private String borrowedBy;

    private String reservedBy;

}
