package cz.cvut.fel.barysole.pojo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.barysole.enums.BookManipulationOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.annotation.Nonnull;
import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "BookManipulationInformation")
@EqualsAndHashCode(of = {"id"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookManipulationInformation implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @Column(name = "bookId", nullable = false)
    private String bookId;

    @Column(name = "userId", nullable = false)
    private String userId;

    @Enumerated(EnumType.STRING)
    private BookManipulationOperation operation;

}
