package cz.cvut.fel.barysole.controller;

import cz.cvut.fel.barysole.exceptions.BadRequest;
import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import cz.cvut.fel.barysole.service.BookManipulationService;
import feign.FeignException;
import feign.RetryableException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/bookManipulation")
public class BookManipulationController {

    private final BookManipulationService bookManipulationService;

    public BookManipulationController(BookManipulationService bookManipulationService) {
        this.bookManipulationService = bookManipulationService;
    }

    @RequestMapping(value = "/borrow", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book was successfully borrowed", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BookDTO.class)
            )),
            @ApiResponse(responseCode = "400", description = "Book cant be borrowed by this user", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Book not or user not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content()),
            @ApiResponse(responseCode = "503", description = "Some of microservices is down", content = @Content()),
    })
    public BookDTO borrowBook(@RequestBody BookAndUserPair bookAndUserPair) {
        try {
            return bookManipulationService.borrowBook(bookAndUserPair);
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FeignException.FeignClientException.ServiceUnavailable|RetryableException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        } catch (BadRequest e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @RequestMapping(value = "/return", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book was successfully returned", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BookDTO.class)
            )),
            @ApiResponse(responseCode = "400", description = "Book cant be returned", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Book not or user not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content()),
            @ApiResponse(responseCode = "503", description = "Some of microservices is down", content = @Content()),
    })
    public BookDTO returnBook(@RequestBody BookAndUserPair bookAndUserPair) {
        try {
            return bookManipulationService.returnBook(bookAndUserPair);
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FeignException.FeignClientException.ServiceUnavailable|RetryableException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        } catch (BadRequest e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @RequestMapping(value = "/reserve", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book was successfully reserved", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BookDTO.class)
            )),
            @ApiResponse(responseCode = "400", description = "Book cant be reserved", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Book or user not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content()),
            @ApiResponse(responseCode = "503", description = "Some of microservices is down", content = @Content()),
    })
    public BookDTO reserveBook(@RequestBody BookAndUserPair bookAndUserPair) {
        try {
            return bookManipulationService.reserveBook(bookAndUserPair);
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FeignException.FeignClientException.ServiceUnavailable|RetryableException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        } catch (BadRequest e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @RequestMapping(value = "/removeReservation", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation was successfully removed", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BookDTO.class)
            )),
            @ApiResponse(responseCode = "400", description = "Reservation can not be removed by this user", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Book or user not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content()),
            @ApiResponse(responseCode = "503", description = "Some of microservices is down", content = @Content()),
    })
    public BookDTO removeReservation(@RequestBody BookAndUserPair bookAndUserPair) {
        try {
            return bookManipulationService.removeReservation(bookAndUserPair);
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FeignException.FeignClientException.ServiceUnavailable|RetryableException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
        } catch (BadRequest e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
