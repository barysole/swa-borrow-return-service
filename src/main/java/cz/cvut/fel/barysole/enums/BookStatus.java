package cz.cvut.fel.barysole.enums;

public enum BookStatus {
    AVAILABLE,
    BORROWED,
    NOT_AVAILABLE,
    RESERVED
}
