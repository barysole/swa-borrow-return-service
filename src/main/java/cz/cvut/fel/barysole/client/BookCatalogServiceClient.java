package cz.cvut.fel.barysole.client;

import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("SwaBookCatalogService")
public interface BookCatalogServiceClient {

    @RequestMapping(value = "/bookCatalog/{id}", method = RequestMethod.GET)
    BookDTO getBook(@PathVariable String id);

    @RequestMapping(value = "/bookCatalog/save", method = RequestMethod.PUT)
    BookDTO saveBook(@RequestBody BookDTO book);

}
