package cz.cvut.fel.barysole.exceptions;

public class BadRequest extends RuntimeException{

    public BadRequest(String message) {
        super(message);
    }
}
